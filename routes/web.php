<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'UsersController@showAuth' );

Route::get('/check_user', 'AuthentificationController@checkEmail')->middleware('only.ajax');
Route::post('/check_user', 'AuthentificationController@checkEmail')->name('check_user');

Route::get('/login', 'AuthentificationController@checkPassword')->middleware('only.ajax');
Route::post('/login', 'AuthentificationController@checkPassword');

Route::get('/dashboard', 'UsersController@showDashboard');

Route::get('/logout', 'AuthentificationController@logout')->middleware('only.ajax');
Route::post('/logout', 'AuthentificationController@logout');

// 
