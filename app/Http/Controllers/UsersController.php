<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use Hash;
use Session;

class UsersController extends Controller {

    public function showAuth (Request $request){
        $session = $request->session();

        if($session->has('email') && $session->has('password')){
            return redirect()->action('UsersController@showDashboard'); 
        }
        return view('login.auth');
    }

    public function showDashboard(Request $request){
        $session = $request->session();
        // $user = $request->user();
        
        if($session->has('email') && $session->has('password')) {
            
            $email = Session::get('email');
            $data = Users::where('email', $email)->first();
            return view('user.user', [
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'id' => $data['id'],
                ]);
        }
        return redirect()->action('UsersController@showAuth');
    }

}
