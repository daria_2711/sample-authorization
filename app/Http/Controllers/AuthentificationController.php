<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Users;
use Debugbar;
// Debugbar::info();

class AuthentificationController extends Controller
{

    // login user

    public function checkEmail (Request $request){
        $valid = $request->validate([
            'email' => 'required|email'
        ]);
        $data = $request->all();
        if($request->has('email')){
            $isUserValid = Users::where('email', $request->input('email'))->exists();
            if($isUserValid) {
                return response()->json(['success' => $data, 'status' => 200]);
            }
        } 
        
        return response()->json(['status' => 'error'], 400);        
    }

    public function checkPassword(Request $request){
        $request->validate(['password' => 'required|min:4']);
        $email = $request->input('email');
        $password = $request->input('password');
        $user = Users::where('email', '=', $email)->first();
        
        if(Hash::check($password, $user->password)) {
            $request->session()->put(['email' => $email, 'password' => $password]);
        } else {
            return response()->json(['error' => 'error'], 401);
        }
    }
    

    // logout user

    public function logout(Request $request){
        $request->session()->flush();
        response()->json(['success' => 'logout', 'status' => 301]);
    }
}
