import axios from "axios";

const instance = axios.create({
    withCredentials: true,
    baseURL: "http://127.0.0.1:8000/"
});

const submit_btn = document.getElementById("submit_btn");
const pass_input = document.getElementById("pass_input");
const email_input = document.getElementById("email_input");
const show_error = document.querySelector(".error");
const btn_logout = document.getElementById("logout");

pass_input && pass_input.classList.add("hide");
pass_input &&
    pass_input.addEventListener("focus", function() {
        show_error.innerHTML = "";
    });
email_input &&
    email_input.addEventListener("focus", function() {
        show_error.innerHTML = "";
    });

// login

submit_btn &&
    submit_btn.addEventListener("click", function(event) {
        show_error.innerHTML = "";
        event.preventDefault();
        if (!email_input.value) {
            show_error.innerHTML = "Please enter your email";
        } else {
            const email = event.currentTarget.parentNode[1].value;
            return instance
                .post(`/check_user`, { email })
                .then(function(response) {
                    if (response.data.status === 200) {
                        pass_input.style.display = "block";
                        submit_btn.addEventListener("click", function(event) {
                            event.preventDefault();
                            if (!pass_input.value) {
                                show_error.innerHTML =
                                    "Please enter your password";
                            } else {
                                const password =
                                    event.currentTarget.parentNode[2].value;
                                return instance
                                    .post(`/login`, { email, password })
                                    .then(function(response) {
                                        if (response.data.status === 200) {
                                            window.location.replace(
                                                "/dashboard"
                                            );
                                        }
                                    })
                                    .catch(function(error) {
                                        console.log(error);
                                        show_error.innerHTML =
                                            "Login OR Pass is invalid";
                                    });
                            }
                        });
                    }
                })

                .catch(function(error) {
                    console.log(error);
                    show_error.innerHTML = "No such user";
                });
        }
    });

// log out

btn_logout &&
    btn_logout.addEventListener("click", function() {
        return instance
            .post(`/logout`)
            .then(response => {
                window.location.href = "/";
                return response.data;
            })
            .catch(function(error) {
                console.log(error);
            });
    });
