@extends('layouts.app')

@section('title'){{$first_name}} @endsection

<div class="container">

  <p>Hello, {{$first_name}} {{$last_name}}</p>
  <p>Here is your email: {{$email}}</p>

  <button class="btn btn-lg btn-danger btn-block" id='logout'>LOG OUT</button>

</div>