@extends('layouts.app')

@section('title')LOGIN @endsection

@section('container')
<div class="container">

  <div class="error"></div>

  <form class="form-signin" action="{{ route('check_user')}}" method="post">
  @csrf
    <input id="email_input" name="email" class="form-control" type="text" placeholder="Enter your email">
    <input id="pass_input" name="password" class="form-control" type="password" placeholder="Enter your password">
    <input id="submit_btn" class="btn btn-lg btn-primary btn-block" type="submit" value="Log In">
  </form>

</div>
@endsection