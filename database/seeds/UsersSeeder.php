<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;
use Carbon\Carbon;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,5) as $index) {
            $email = $faker->email;
            $password = $faker->password();
            echo PHP_EOL . 'Email: ' . $email . PHP_EOL;
            echo PHP_EOL . 'Password: ' . $password . PHP_EOL,

	        DB::table('users')->insert([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $email,
                'password' => Hash::make($password),
                'created_at' => Carbon::now(),
	        ]);
        }
    }
}
